//Use ROS <might be disable if don't need ROS feature>


#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#ifdef _WIN32
	#include <conio.h>

#endif
#include <fstream>

#ifdef ROS
	#include "ros/ros.h"
#endif

//WARNING : BOOST library needed to be installed
#include "boost/thread/thread.hpp"

#include <thread>
#include <iostream>


#include "serial/serial.h"
#include "commands.h"
#include "ubot.h"


#include <unistd.h>


//Using namespace
using namespace std;

void upload();
void download(); 
void tf_broadcast();

Ubot *ubot  = new Ubot();

int main(int argc, char *argv[])
{

	//Add this as Node to ROS
	ubot->initROS(argc,argv);

	///Send Ubot initial commands through RS-232
    if(ubot->initUbot()==-1)
	{
    	ROS_INFO(" %s","initROS Failed\n");
    	ROS_INFO("Program Ended");
    	return -1;
    }

    //Creat thread to translate informations from Ubot to ROS
	boost::thread thread_handle_upload(upload);
	//Creat thread to translate informations from ROS to Ubot
	boost::thread thread_handle_download(download);

	//NOT USED
	//boost::thread thread_handle_tf(tf_broadcast);


	//while the node is NOT terminated
	while(ros::ok())
	{
		//Print out the 
		ubot->mycmd.print_Std_SIP();
		sleep(1);
	}

	//Send Ubot Reset Command before node exit
	ubot->my_serial->write(ubot->mycmd.ubot_cmd_RESET(),6);

	//Release serial port resources
	delete ubot->my_serial;

	return 0;
}




void upload()
{
	//Message for odemetery data from Ubot
	nav_msgs::Odometry *msg = new nav_msgs::Odometry();

	//Message for pointcloud which translated from sonar data collected from Ubot
	sensor_msgs::PointCloud *myCloud  = new sensor_msgs::PointCloud;
	while(1)
	{
		//Check if we are able to read data from serial port (connected to Ubot)
		if (ubot->my_serial->waitReadable())
		{
			//Check if there contain any packet in the bytes received 
			uint8_t pdata[100];
			if (ubot->ubot_msgs_scan(pdata)==0)

			#ifdef ROS
				//Fill the data into ros format
				//Used the packet to generate corresponding message
				ubot->mycmd.ubot_ros_parse_and_convert_odom(pdata,msg);
			#else
				//Use following method to get date (Odom_Data)
				//ubot_ros_parse_and_convert_odom(pdata,Odom_Data_msg)
			#endif

			//Push the generated message to topic(#define topicName_odom)
			ubot->Publisher->publish(*msg);
		}
		
		//Convert the received sonar data to pointcloud message
		ubot->mycmd.ubot_ros_sonar_to_pointcloud(myCloud);
		//Push the generated message to topic("sonar_pc")
		ubot->pc_Publisher->publish(*myCloud);

		//Sleep thread for 20 ms
		boost::posix_time::milliseconds sleeptime(20);
	    boost::this_thread::sleep(sleeptime);
	}
}


void download()
{
	ROS_INFO("download thread Called");
	while(1)
	{
		ros::spinOnce();
		boost::posix_time::milliseconds sleeptime(20);
    	boost::this_thread::sleep(sleeptime);
	}
}


//!!!!IMPLEMENTATION is necessary
void ros_cmd_handle(const geometry_msgs::Twist& msg)             //Decleared in ubot.h
{
	//Convert ROS command to Ubot format and put into queue
	int queue_length = ubot->mycmd.ros_ubot_commands_convert(msg);
	
	//Write the queue to Ubot
	for (int i = 0; i < queue_length; ++i)
		ubot->my_serial->write(ubot->mycmd.commands_queue[i]+1, ubot->mycmd.commands_queue[i][0]);
}


void tf_broadcast()
{
	static tf::TransformBroadcaster broadcaster;
	while(1)
	{
    boost::posix_time::milliseconds sleeptime(20);
    boost::this_thread::sleep(sleeptime);
	}
}
