# Contents
The interface package between UBOT and ROS

# Features
+ Read robot pose and other infomation and publish onto ROS topics
+ Subscribe message from command topics and convert it low-level packet for UBOT sent through serial port.