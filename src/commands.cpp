#include "commands.h"

#include <cstdlib>
#include "stdlib.h"
#include "stdio.h"
#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <vector>

using namespace std;

Command::Command()
{
    this->length = 0;

    static unsigned char s_packet[PACKET_SIZE];
    static unsigned char r_packet[PACKET_SIZE];

    ////Initial pointer
    this->r_current = (uint8_t*) r_packet;
    this->s_head = (uint8_t*) s_packet;
    this->r_head = (uint8_t*) r_packet;
    this->pdata = (uint16_t*) &(this->s_head[5]);
    CONF_SIP.robot_type  =(uint8_t*)this->str_robotType;
    CONF_SIP.subtype     =(uint8_t*)this->str_subType;
    CONF_SIP.serialnum   =(uint8_t*)this->str_serialNum;
    CONF_SIP.name        =(uint8_t*)this->str_name;

     //Clean space
    memset(this->r_head,0,PACKET_SIZE);
    memset(this->s_head,0,PACKET_SIZE);
    memset(&STD_SIP,0,sizeof(sip_standar));
  
    //Clean Flag
    FLAG_headFounded = 0;
    FLAG_fullLengthFounded = 0;
    FLAG_lastIs_FA = 0;
    FLAG_getLength = 0;

    s_packet[0] = 0xFA;
    s_packet[1] = 0xFB;
}


uint8_t* Command::ubot_cmd_SYNC0()
{
    this->clean();
    this->length = ccp_msgCode[0][0];
    this->setValue_l3(0);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SYNC1()
{
    this->clean();
    this->length = ccp_msgCode[1][0];
    this->setValue_l3(1);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SYNC2()
{
    this->clean();
    this->length = ccp_msgCode[2][0];
    this->setValue_l3(2);
    this->routine();

    return this->s_head;
}


uint8_t* Command::ubot_cmd_WTF0()
{
    this->clean();
    this->length = 6;
    unsigned char packet[] = {0xfa,0xfb,0x06,0x01,0x3b,0x01,0x00,0x02,0x3b};
    memcpy(this->s_head,packet,9);

	return this->s_head;
}


uint8_t* Command::ubot_cmd_WTF1()
{
    this->clean();
    this->length = 6;
    unsigned char packet[] = {0xfa,0xfb,0x06,0x13,0x3b,0x01,0x00,0x14,0x3b};
    memcpy(this->s_head,packet,9);

	return this->s_head;
}


uint8_t* Command::ubot_cmd_OPEN()					//1
{
    this->clean();
    this->length = ccp_msgCode[1][0];
    this->setPartValue(1,0);

    ((uint16_t*)this->s_head)[3] = calCheckSum((unsigned char *)this->s_head);
    this->routine();
    
    return this->s_head;
}



uint8_t* Command::ubot_cmd_ENABLE()					//4
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[4][0];	 //Set class lenght
    
    //Assign value
    this->setPartValue(4,5);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SETA(int linearAcceleration)			//5
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[5][0]; //Set class lenght

    //Assign value
    this->setPartValue(5,linearAcceleration);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SETV(int maxLinearSpeed)				//6
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[6][0]; //Set class lenght

    //Assign value
    this->setPartValue(6,maxLinearSpeed);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SETO()								//7
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[7][0]; //Set class lenght

    //Assign value
    this->setValue_l3(7);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_MOVE(int distance)					//8
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[8][0]; //Set class lenght

    //Assign value
    this->setPartValue(8,distance);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_ROTATE(int degree)					//9
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[9][0]; //Set class lenght

    //Assign value
    this->setPartValue(9,degree);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SETRV (int rotateSpeed)				//10
{
    this->clean();
    this->length = ccp_msgCode[10][0]; //Set class lenght

    //Assign value
    this->setPartValue(10,rotateSpeed);
    this->routine();
    
    return this->s_head;

}


uint8_t* Command::ubot_cmd_VEL (int speed)						//11
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[11][0]; //Set class lenght

    //Assign value
    this->setPartValue(11,speed);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_HEAD (int degree)					//12
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[12][0]; //Set class lenght

    //Assign value
    this->setPartValue(12,degree);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_DHEAD (int degree)					//13
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[13][0]; //Set class lenght

    //Assign value
    this->setPartValue(13,degree);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_ENCODER (bool returnVal)				//19
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[19][0]; //Set class lenght

    //Assign value
    this->setPartValue(19,returnVal);
    this->routine();
    
    return this->s_head;
}



uint8_t* Command::ubot_cmd_RVEL (int rspeed)					//21
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[21][0]; //Set class lenght

    //Assign value
    this->setPartValue(21,rspeed);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SETRA (int degree)					//23
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[23][0]; //Set class lenght

    //Assign value
    this->setPartValue(23,degree);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_SONAR (bool sonar1,bool sonar2)		//28
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[28][0]; //Set class lenght

    //Assign value
    int bits = 0;
    bits = sonar1+sonar2<<1;
    this->setPartValue(28,bits);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_STOP ()								//29
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[29][0]; //Set class lenght

    //Assign value
    this->setValue_l3(29);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_E_STOP ()							//55
{
    //Preset
    this->clean();
    this->length = ccp_msgCode[55][0]; //Set class lenght

    //Assign value
    this->setValue_l3(55);
    this->routine();
    
    return this->s_head;
}


uint8_t* Command::ubot_cmd_RESET (){
this->clean();
    this->length = ccp_msgCode[253][0];
    this->setValue_l3(253);
    this->routine();
    
    return this->s_head;

}

/*void register_std_SIP_handle(void (*fn)())
{

}*/


void Command::setValue_l3(int msg_Num)
{
    for(uint8_t count =0;count<2;count++)
        this->jump(count+1)[0] = ccp_msgCode[msg_Num][count];
   
    ((uint16_t*)(this->s_head+4))[0] = calCheckSum(this->s_head);
}


//Parameter: pass the read length

int Command::readAsPacket(int read_length)
{
    static int countRemain = read_length-1;
    static int charToRead = 0;
    static char temp[512];
    static char *temp_current = temp+3;
    FLAG_fullLengthFounded = 0;
    r_current = this->r_head; 				// Always put it to start point when begin
    bool reCheck = 1;
   
    while(reCheck)
	{
        if (!FLAG_headFounded)
		{
            while(countRemain)
			{
                if ((this->r_current[0]==0xfa)&&(this->r_current[1]==0xfb))
				{
                    FLAG_headFounded = 1;
                    
                    if (countRemain>0)
                        charToRead = r_current[2]; // 0xfa 0xfb "0xxx"
                    else
                        FLAG_getLength = 1; // length data is at the head of next data
                    
					#ifdef DEBUG
                        cout<<"Head Found"<<endl;
                    #endif
                    break;
                }
                r_current++;
                countRemain--;
            }
            
            if ((!FLAG_headFounded)&&(this->r_current[read_length-1]!=0xfa))	//Discard data
			{
                cout<<"Data Discarded"<<endl;
                return -1;
            }
			else	//else have been handled in while
			{
                if (this->r_current[read_length-1]==0xfa)
				{
                    FLAG_lastIs_FA = 1;
                    temp[0] = 0xfa;
                }
            }
        }
		else
		{
            if (FLAG_lastIs_FA ==1)			 //Category1: fa fb wsa splited
			{
                if ((this->r_head[0])==0xfb)
				{
                    FLAG_headFounded =1;
                    r_current = this->r_head+2;
                    charToRead = (this->r_head +1)[0];
                    temp[1] = 0xfb;
                    temp[2] = charToRead;
                    //Handle data here
                }
                else
				{
                    FLAG_lastIs_FA = 0;                    //Keep Checking
                }
            }
            else if (FLAG_getLength)			//Category2:length data is at the head of data
			{
                charToRead = this->r_head[0];
                FLAG_getLength = 0;
               						/* Put the remained data into temp
                					   Data read is longer then needed 
              						   TODO: We have to reuse the rest data */
              						   
                if ((read_length-1) > charToRead)
				{
                    				//temp_current point at the start of contents
                    				//r_current should already moved to the start of contents
                    memcpy(temp_current,r_current,charToRead);
                    FLAG_fullLengthFounded = 1 ;
                    FLAG_headFounded = 0;
                    FLAG_lastIs_FA = 0;
                    FLAG_getLength = 0;
                    charToRead -= charToRead;
                    temp_current += charToRead;
                    reCheck = 1;
                    return 1;
                }
				else
				{
                    memcpy(temp_current,r_current,read_length-1);//Haven't Get FULL data
                    charToRead -= read_length;			
                    temp_current +=  read_length;
                    reCheck = 0;
                    return 0;
                }

            }
            //Category3: Simply read data
            else
			{
                if (read_length>charToRead)
				{
                    memcpy(temp_current,r_current,charToRead);
                    FLAG_fullLengthFounded = 1 ;
                    FLAG_headFounded = 0;
                    FLAG_lastIs_FA = 0;
                    FLAG_getLength = 0;
                    charToRead -= charToRead;
                    temp_current += charToRead;
                    reCheck = 1;
                    return 1;
                }
				else
				{
                    memcpy(temp_current,r_current,read_length);//Haven't Get FULL data
                    charToRead -= read_length;
                    temp_current +=  read_length;
                    reCheck = 0;
                    return 0;
                }
            }
        }
	}
}

// Function : Parse different kind of data returned from Ubot 
// Return : -1 if length does not match content or CheckSum is wrong
//          1 : std_SIP is ready 2:Config_SIP is ready 3:Encoder_SIP is ready
//          0 none of data is ready yet

int Command::parse(uint8_t* pPacketData)
{
    int length = pPacketData[2];
    uint8_t *pCheckSum = pPacketData+3+length-2;
    
    memset(&STD_SIP, 0, sizeof(STD_SIP));   					 //Clean data

    if ((pPacketData[0]!=0xfa)||(pPacketData[1] != 0xfb))		//Checking if the data is begined with fa fb
	{
        #ifdef DEBUG
            cout<<"Head is incorrect (not begin with fa fa)"<<endl;
            cout<<"Return -1"<<endl; 
        #endif
        return -1;
    }

    //Check if checkSum is correct
    //TODO: Check if checkSum needed to be swaped
    if (this->calCheckSum(pPacketData) == (pCheckSum[0]*256+pCheckSum[1]))
	{
        if ((pPacketData[3] == 0x32)||(pPacketData[3] == 0x33))  //Ubot Status [0x32->Ubot Stop] [0x33->Ubot moving]
		{
			this->STD_SIP.start             = pPacketData[0]+(pPacketData[1])*256;
            this->STD_SIP.length            = pPacketData[2];
            this->STD_SIP.status            = pPacketData [3];
            this->STD_SIP.x_pos             = pPacketData[4] + pPacketData[5]*256;
            this->STD_SIP.y_pos             = pPacketData[6] + pPacketData[7]*256;
            this->STD_SIP.th_pos            = (pPacketData[8] + pPacketData[9]*256)*(2*PI/4096.0);
            this->STD_SIP.l_vel             = pPacketData[10] + pPacketData[11]*256;
            this->STD_SIP.r_vel             = pPacketData[12] + pPacketData[13]*256;
			/*
			printf("pPacketData[4]:%d\r\n", pPacketData[4]);
			printf("pPacketData[5]:%d\r\n", pPacketData[5]);
			printf("pPacketData[6]:%d\r\n", pPacketData[6]);
			printf("pPacketData[7]:%d\r\n", pPacketData[7]);
			*/
			#ifdef DEBUG
                 cout<<"x_pos: "<< this->STD_SIP.x_pos  <<endl;
				 cout<<"y_pos: "<< this->STD_SIP.y_pos  <<endl;
				 cout<<"th_pos: "<< this->STD_SIP.th_pos  <<endl;
				 cout<<"l_vel: "<< this->STD_SIP.l_vel  <<endl;
				 cout<<"r_vel: "<< this->STD_SIP.r_vel  <<endl;
            #endif

            this->STD_SIP.battery           = pPacketData[14];
            this->STD_SIP.stall_and_bumpers = pPacketData[15]+pPacketData[16]*256;
            this->STD_SIP.th_pos_360        = pPacketData[17]+pPacketData[18]*256;
            this->STD_SIP.flag              = pPacketData[19]+pPacketData[20]*256;
            this->STD_SIP.compass           = pPacketData[21];
            this->STD_SIP.sonar_count       = pPacketData[22];

            //Varying Section
            int index = 0;
            int sonarCount = this->STD_SIP.sonar_count;
            for (int count = 0;count < sonarCount ;count++)
			{
				index = 23 + count*3;
                switch(pPacketData[index])
				{
					case 0 : {this->STD_SIP.sonar_reading0  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 1 : {this->STD_SIP.sonar_reading1  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 2 : {this->STD_SIP.sonar_reading2  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 3 : {this->STD_SIP.sonar_reading3  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 4 : {this->STD_SIP.sonar_reading4  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 5 : {this->STD_SIP.sonar_reading5  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 6 : {this->STD_SIP.sonar_reading6  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 7 : {this->STD_SIP.sonar_reading7  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 8 : {this->STD_SIP.sonar_reading8  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 9 : {this->STD_SIP.sonar_reading9  = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 10: {this->STD_SIP.sonar_reading10 = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 11: {this->STD_SIP.sonar_reading11 = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 12: {this->STD_SIP.sonar_reading12 = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 13: {this->STD_SIP.sonar_reading13 = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 14: {this->STD_SIP.sonar_reading14 = pPacketData[index+1]+pPacketData[index+2]*256;}
                    case 15: {this->STD_SIP.sonar_reading15 = pPacketData[index+1]+pPacketData[index+2]*256;}       
                 }
            }
                    
            index +=3;
            this->STD_SIP.grip_state        = pPacketData [index]; index++;
            this->STD_SIP.ad_portnum        = pPacketData [index]; index++;
            this->STD_SIP.ad_reading        = pPacketData [index]; index++;
            this->STD_SIP.digital_input     = pPacketData [index]; index++;
            this->STD_SIP.digital_output    = pPacketData [index]; index++;
            this->STD_SIP.batteryx10        = pPacketData [index]; index++;
            this->STD_SIP.chargestate       = pPacketData [index]; index++;

            //TODO: Check if this is correct 
            this->STD_SIP.checksum          = pPacketData [index]*256 + pPacketData[index+1]; 
             
            #ifdef DEBUG
                 cout<<"Parse Finish"<<endl;
            #endif
            return 1;
        }
		else if (pPacketData[3]==0x20)            //Configuration SIP
		{
            int position = 4 ;
            
			//Read String 1
            uint8_t count = 0;
            
            while(pPacketData[position]!=0)
			{
                count = 0;
                str_robotType[count] = pPacketData[position];
                count++; 
                position++;
            }
            
            str_robotType[count] = 0;
            position++;

            //Read String 2
            while(pPacketData[position]!=0)
			{
                count = 0;
                str_subType[count] = pPacketData[position];
                count++; 
                position++;
            }
            
            str_subType[count] = 0;
            position++;

            //Read String 3
            while(pPacketData[position]!=0)
			{
                count = 0;
                str_serialNum[count] = pPacketData[position];
                count++; 
                position++;
            }
            
            str_serialNum[count] = 0;
            position++;

            //Section 1//////////////////////////
            CONF_SIP.rotveltop = pPacketData [position+1] ; 
            CONF_SIP.transveltop = pPacketData [position+3] ; 

            position += 11; //move to the head of 'string 4'(NAME)
            ///////////////////////////////////

            //Read String 4
            while(pPacketData[position]!=0)
			{
                count = 0;
                str_name[count] = pPacketData[position];
                count++; 
                position++;
            }
            str_name[count] = 0;
            position++;

            //Section 2////////////////////////
            CONF_SIP.rotvelmax    = pPacketData[position+21];
            CONF_SIP.transvelmax  = pPacketData[position+23];
            ///////////////////////////////////

            CONF_SIP.checksum =  pCheckSum[0]*256 + pCheckSum[1];

            return 2;
        }
		else if (pPacketData[3]==0x90)            //Encoder SIP
        {
            this->ENC_SIP.start = pPacketData[0] + pPacketData[1]*256;
            this->ENC_SIP.length = pPacketData[2];
            this->ENC_SIP.type = pPacketData[3];

            ////TODO: Check if coversion is correct//////////////
            this->ENC_SIP.l_encoder = pPacketData[4]+pPacketData[5]*256+(pPacketData[6]+pPacketData[7]*256)*256;
            this->ENC_SIP.r_encoder = pPacketData[8]+pPacketData[9]*256+(pPacketData[10]+pPacketData[11]*256)*256;
            /////////////////////////////////////////////////////
            this->ENC_SIP.checksum = pPacketData[12]*256 + pPacketData[13];
            return 3;
        }
        else				//Not Belong to any of these data type
		{
           #ifdef DEBUG
               cout<<"!!!!Not belong to any type of SIP"<<endl;
           #endif
           return -1;
        }
    }
    else					//checksum error
	{
        #ifdef DEBUG
            cout<<"!!!!CheckSum is Not Correct"<<endl;
        #endif
		return -1;
    }
}


#ifdef ROS

int Command::ros_ubot_commands_convert(const geometry_msgs::Twist& msg)             //Return queue lenght
{
    //Stop
    //Set rotation vel
        int vel = msg.angular.z*(180.0/PI);
        this->ubot_cmd_SETRV(abs(vel));
        memcpy(commands_queue[0]+1,this->s_head,this->totalLength());
        commands_queue[0][0] = this->totalLength();

    //DHEAD
        int rotateAngle;
        if (vel==0)
            rotateAngle = 0;
        else if (vel>0)
            rotateAngle = ROTATE_STEP_ANGLE;
        else
            rotateAngle = -ROTATE_STEP_ANGLE;

        this->ubot_cmd_DHEAD(rotateAngle);
        memcpy(commands_queue[1]+1,this->s_head,this->totalLength());
        commands_queue[1][0] = this->totalLength();

    //Set velocity
        int vel2 = msg.linear.x*1000;
        this->ubot_cmd_VEL(vel2);
        memcpy(commands_queue[2]+1,this->s_head,this->totalLength());
        commands_queue[2][0] = this->totalLength();
    
    //GO GO GO
        int distance;
        if (vel2 ==0)
            distance = 0;
        else if (vel2>0)
            distance = LINEAR_STEP_DISTANCE;
        else
            distance = -LINEAR_STEP_DISTANCE;

        this->ubot_cmd_MOVE(distance);//mm
        memcpy(commands_queue[3]+1,this->s_head,this->totalLength());
        commands_queue[3][0] = this->totalLength();

        return 4;
}

int Command::ubot_ros_parse_and_convert_odom(uint8_t *pPacketData, nav_msgs::Odometry *odom)
{
    int returnVal = 0;
    returnVal = this->parse(pPacketData);
    if(returnVal<0) return -1;
     ros::Time current_time, last_time;
     current_time = ros::Time::now();
     last_time = ros::Time::now();
    
     static tf::TransformBroadcaster odom_broadcaster;
     //TODO: Check if it's ok to work with return theta
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(this->STD_SIP.th_pos);

///////////////////////////////////////////////////////////////////////////////////////
     //first, we'll publish the transform over tf
     geometry_msgs::TransformStamped odom_trans;
     odom_trans.header.stamp = current_time;
     odom_trans.header.frame_id = "odom";
     odom_trans.child_frame_id = "base_link";
 
     odom_trans.transform.translation.x = this->STD_SIP.x_pos/1000.0;
     odom_trans.transform.translation.y = this->STD_SIP.y_pos/1000.0;
     odom_trans.transform.translation.z = 0.0;
     odom_trans.transform.rotation = odom_quat;
 
     //send the transform
     odom_broadcaster.sendTransform(odom_trans);

///////////////////////////////////////////////////////////////////////////////////////
     odom->header.stamp = current_time;
     odom->header.frame_id = "odom";
 
     //set the position
     odom->pose.pose.position.x = this->STD_SIP.x_pos/1000.0;
     odom->pose.pose.position.y = this->STD_SIP.y_pos/1000.0;
     odom->pose.pose.position.z = 0.0;
     odom->pose.pose.orientation = odom_quat;
 
     //set the velocity
     odom->child_frame_id = "base_link";
     float speed = (this->STD_SIP.l_vel + this->STD_SIP.r_vel)/2.0/1000.0;
     odom->twist.twist.linear.x = speed * cos (this->STD_SIP.th_pos+90);
     odom->twist.twist.linear.y = speed * sin (this->STD_SIP.th_pos+90);
     odom->twist.twist.angular.z = ((this->STD_SIP.r_vel - this->STD_SIP.l_vel)/wheelWitdth)/1000.0;
     return returnVal;
}   
#else
int Command::ubot_ros_parse_and_convert_odom(uint8_t *pPacketData, Odom_Data *OutputData)
{    
    int returnVal = 0;

     //Parse the Received Packet 
     returnVal = this->parse(pPacketData);
     //Check if parse operation is successful
     if(returnVal<0) return -1;

     if(returnVal == 1){
        //NOTICE: Unit
         OutputData->x = this->STD_SIP.x_pos;
         OutputData->y = this->STD_SIP.y_pos;
         OutputData->theta =  this->STD_SIP.th_pos;

         double speed = (this->STD_SIP.l_vel + this->STD_SIP.r_vel)/2.0/1000.0;
         OutputData->Vx = speed * cos (this->STD_SIP.th_pos+90);
         OutputData->Vy = speed * sin (this->STD_SIP.th_pos+90);
         OutputData->angular_z = ((this->STD_SIP.r_vel - this->STD_SIP.l_vel)/wheelWitdth)/1000.0;
    }
    return returnVal;
}




int Command::ros_ubot_commands_convert(double velocity_x ,double angular_z)             //Return queue lenght
{
    //Stop
    //Set rotation vel
        int vel = angular_z*(180.0/PI);
        this->ubot_cmd_SETRV(abs(vel));
        memcpy(commands_queue[0]+1,this->s_head,this->totalLength());
        commands_queue[0][0] = this->totalLength();

    //DHEAD
        int rotateAngle;
        if (vel==0)
            rotateAngle = 0;
        else if (vel>0)
            rotateAngle = ROTATE_STEP_ANGLE;
        else
            rotateAngle = -ROTATE_STEP_ANGLE;

        this->ubot_cmd_DHEAD(rotateAngle);
        memcpy(commands_queue[1]+1,this->s_head,this->totalLength());
        commands_queue[1][0] = this->totalLength();

    //Set velocity
        int vel2 = (int)(velocity_x*1000.0);
        this->ubot_cmd_VEL(vel2);
        memcpy(commands_queue[2]+1,this->s_head,this->totalLength());
        commands_queue[2][0] = this->totalLength();
    
    //GO
        int distance;
        if (vel2 ==0)
            distance = 0;
        else if (vel2>0)
            distance = LINEAR_STEP_DISTANCE;
        else
            distance = -LINEAR_STEP_DISTANCE;

        this->ubot_cmd_MOVE(distance);//mm
        memcpy(commands_queue[3]+1,this->s_head,this->totalLength());
        commands_queue[3][0] = this->totalLength();

        return 4;
}

#endif 




//Function : Transform the distance data to Pointcloud[ROS Format]
int Command::ubot_ros_sonar_to_pointcloud(sensor_msgs::PointCloud *theCloud)
{ 
    int returnVal = 0;


    theCloud->header.stamp = ros::Time::now();
    theCloud->header.frame_id = "base_link";

    theCloud->points.resize(16+360);
    

    //Transform the sonar distance into coordinate (move_base)  [Point Cloud]
    
     theCloud->points[0].x = 0;  ///TODO: Check for this 
     theCloud->points[0].y = SCALE_sonar_point * (this->STD_SIP.sonar_reading0 + 150) * -1;
     theCloud->points[0].z = SCALE_sonar_point * 250;  
    
     theCloud->points[1].x = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading1) *cos(-PI*45/180);
     theCloud->points[1].y = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading1) *sin(-PI*45/180);
     theCloud->points[1].z = SCALE_sonar_point * 250;

     theCloud->points[2].x = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading2) *cos(-PI*30/180);
     theCloud->points[2].y = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading2) *sin(-PI*30/180);
     theCloud->points[2].z = SCALE_sonar_point * 250;

     theCloud->points[3].x = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading3) *cos(-PI*15/180);
     theCloud->points[3].y = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading3) *sin(-PI*15/180);
     theCloud->points[3].z = SCALE_sonar_point * 250;

     theCloud->points[4].x = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading4) *cos(PI*15/180);
     theCloud->points[4].y = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading4) *sin(PI*15/180);
     theCloud->points[4].z = SCALE_sonar_point * 250;

     theCloud->points[5].x = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading5) *cos(PI*30/180);
     theCloud->points[5].y = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading5) *sin(PI*30/180);
     theCloud->points[5].z = SCALE_sonar_point * 250;

     theCloud->points[6].x = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading6) *cos(PI*45/180);
     theCloud->points[6].y = SCALE_sonar_point * (200 + this->STD_SIP.sonar_reading6) *sin(PI*45/180);
     theCloud->points[6].z = SCALE_sonar_point * 250;

     theCloud->points[7].x = 0;  
     theCloud->points[7].y = SCALE_sonar_point * (this->STD_SIP.sonar_reading7 + 150);
     theCloud->points[7].z = SCALE_sonar_point * 250;  

     theCloud->points[8].x = SCALE_sonar_point * -250;  
     theCloud->points[8].y = SCALE_sonar_point * (this->STD_SIP.sonar_reading8 + 150);
     theCloud->points[8].z = SCALE_sonar_point * 250;  

     //theCloud->points[9].x = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading9) *cos(PI*135/180)-250);
     //theCloud->points[9].y = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading9) *sin(PI*135/180)-250);
     //theCloud->points[9].z = SCALE_sonar_point * 250;
    theCloud->points[9].x = SCALE_sonar_point * ((170  + this->STD_SIP.sonar_reading9) *cos(PI*135/180)-250);
    theCloud->points[9].y = SCALE_sonar_point * ((160  + this->STD_SIP.sonar_reading9) *sin(PI*135/180)-250);
    theCloud->points[9].z = SCALE_sonar_point * 250;
    //theCloud->points[10].x = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading10) *cos(PI*150/180)-250);
    // theCloud->points[10].y = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading10) *sin(PI*150/180)-250);
    // theCloud->points[10].z = SCALE_sonar_point * 250;
    theCloud->points[10].x = SCALE_sonar_point * ((170  + this->STD_SIP.sonar_reading10) *cos(PI*150/180)-250);
    theCloud->points[10].y = SCALE_sonar_point * ((160  + this->STD_SIP.sonar_reading10) *sin(PI*150/180)-250);
    theCloud->points[10].z = SCALE_sonar_point * 250;
     //theCloud->points[11].x = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading11) *cos(PI*165/180)-250);
     //theCloud->points[11].y = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading11) *sin(PI*165/180)-250);
     //theCloud->points[11].z = SCALE_sonar_point * 250;
    theCloud->points[11].x = SCALE_sonar_point * ((170  + this->STD_SIP.sonar_reading11) *cos(PI*165/180)-250);
    theCloud->points[11].y = SCALE_sonar_point * ((160  + this->STD_SIP.sonar_reading11) *sin(PI*165/180)-250);
    theCloud->points[11].z = SCALE_sonar_point * 250;
    
     theCloud->points[12].x = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading12) *cos(PI*195/180)-250);
     theCloud->points[12].y = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading12) *sin(PI*195/180)-250);
     theCloud->points[12].z = SCALE_sonar_point * 250;

     theCloud->points[13].x = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading13) *cos(PI*210/180)-250);
     theCloud->points[13].y = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading13) *sin(PI*210/180)-250);
     theCloud->points[13].z = SCALE_sonar_point * 250;

     theCloud->points[14].x = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading14) *cos(PI*225/180)-250);
     theCloud->points[14].y = SCALE_sonar_point * ((200  + this->STD_SIP.sonar_reading14) *sin(PI*225/180)-250);
     theCloud->points[14].z = SCALE_sonar_point * 250;

     theCloud->points[15].x = SCALE_sonar_point * -250;  
     theCloud->points[15].y = SCALE_sonar_point * (this->STD_SIP.sonar_reading15 + 150) * -1;
     theCloud->points[15].z = SCALE_sonar_point * 250;  


     //WARNING:Manually added point to clean the map [Should be removed in other project]
     //--------------------------------------------------------------------------------------------//
     int radius = 5;//meter
     for(int count =0;count<360 ;count++){
     	theCloud->points[16+count].x = radius * cos(count*PI/180);  
     	theCloud->points[16+count].y = radius * sin(count*PI/180);
     	theCloud->points[16+count].z = SCALE_sonar_point * 250;  
        
     }
     //--------------------------------------------------------------------------------------------//
     return 0;
}


void Command::setPartValue(int msg_Num,int data)
{
    for(uint8_t count =0;count<3;count++)
        this->jump(count+1)[0] = ccp_msgCode[msg_Num][count];
    
    this->pdata[0] = data;
    ((uint16_t*)this->jump(5))[0] = calCheckSum(this->s_head);
}


void Command::print_Raw(int limit)
{
    for(int count = 0; count<=limit; count++)
        printf("%x ",(this->s_head+count)[0]);
    
    printf("\n");
}

void Command::print_Raw_r(int limit)
{
    for(int count = 0; count<=limit; count++)
        printf("%x ",(this->r_head+count)[0]);
        
    printf("\n");
}
void Command::print_Raw_Std_SIP()
{
    uint8_t *p = (uint8_t*)&this->STD_SIP;
    for(int count = 0; count<80; count++)
	{
        printf("%3x ",p[count]);
        if (count%10==9)
        {
            printf("\n");
        }
    }
    printf("\n");
}


void Command::print_Std_SIP()
{
#ifdef DEBUG
printf("%-20s"," start   "); printf("%-4x (hex)\n",  this->STD_SIP.  start   );
printf("%-20s"," length  "); printf("%-4x (hex)\n",  this->STD_SIP.  length  );
printf("%-20s"," status  "); printf("%-4x (hex)\n",  this->STD_SIP.  status  );
#endif
printf("%-20s"," x_pos   "); printf("%-4d\n",    this->STD_SIP.  x_pos   );
printf("%-20s"," y_pos   "); printf("%-4d\n",    this->STD_SIP.  y_pos   );
printf("%-20s"," th_pos  "); printf("%-4f\n",    this->STD_SIP.  th_pos  );

printf("%-20s"," sonar_count "); printf("%-4d\n",    this->STD_SIP.  sonar_count );
printf("%-20s"," sonar_reading0  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading0  );
printf("%-20s"," sonar_reading1  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading1  );
printf("%-20s"," sonar_reading2  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading2  );
printf("%-20s"," sonar_reading3  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading3  );
printf("%-20s"," sonar_reading4  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading4  );
printf("%-20s"," sonar_reading5  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading5  );
printf("%-20s"," sonar_reading6  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading6  );
printf("%-20s"," sonar_reading7  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading7  );
printf("%-20s"," sonar_reading8  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading8  );
printf("%-20s"," sonar_reading9  "); printf("%-4d\n",    this->STD_SIP.  sonar_reading9  );
printf("%-20s"," sonar_reading10 "); printf("%-4d\n",    this->STD_SIP.  sonar_reading10 );
printf("%-20s"," sonar_reading11 "); printf("%-4d\n",    this->STD_SIP.  sonar_reading11 );
printf("%-20s"," sonar_reading12 "); printf("%-4d\n",    this->STD_SIP.  sonar_reading12 );
printf("%-20s"," sonar_reading13 "); printf("%-4d\n",    this->STD_SIP.  sonar_reading13 );
printf("%-20s"," sonar_reading14 "); printf("%-4d\n",    this->STD_SIP.  sonar_reading14 );
printf("%-20s"," sonar_reading15 "); printf("%-4d\n",    this->STD_SIP.  sonar_reading15 );
#ifdef DEBUG
printf("%-20s"," l_vel   "); printf("%-4d\n",    this->STD_SIP.  l_vel   );
printf("%-20s"," r_vel   "); printf("%-4d\n",    this->STD_SIP.  r_vel   );
printf("%-20s"," battery "); printf("%-4d\n",    this->STD_SIP.  battery );
printf("%-20s"," stall_and_bumpers   "); printf("%-4d\n",    this->STD_SIP.  stall_and_bumpers   );
printf("%-20s"," th_pos_360  "); printf("%-4d\n",    this->STD_SIP.  th_pos_360  );
printf("%-20s"," flag    "); printf("%-4d\n",    this->STD_SIP.  flag    );
printf("%-20s"," compass "); printf("%-4d\n",    this->STD_SIP.  compass );


printf("%-20s"," grip_state  "); printf("%-4d\n",    this->STD_SIP.  grip_state  );
printf("%-20s"," ad_portnum  "); printf("%-4d\n",    this->STD_SIP.  ad_portnum  );
printf("%-20s"," ad_reading  "); printf("%-4d\n",    this->STD_SIP.  ad_reading  );
printf("%-20s"," digital_input   "); printf("%-4d\n",    this->STD_SIP.  digital_input   );
printf("%-20s"," digital_output  "); printf("%-4d\n",    this->STD_SIP.  digital_output  );
printf("%-20s"," batteryx10  "); printf("%-4d\n",    this->STD_SIP.  batteryx10  );
printf("%-20s"," chargestate "); printf("%-4d\n",    this->STD_SIP.  chargestate );
printf("%-20s"," checksum    "); printf("%-4x (hex)\n",  this->STD_SIP.  checksum    );
#endif
}

void Command::print_Config_SIP()
{
cout<<"Configuration SIP (as follow):"<<endl;
printf("%-20s"," start   "); printf("%-4d\n",    this->CONF_SIP. start   );
printf("%-20s"," length  "); printf("%-4d\n",    this->CONF_SIP. length  );
printf("%-20s"," type    "); printf("%-4d\n",    this->CONF_SIP. type    );
printf("%-20s"," *robot_type "); printf("%-8s\n",    this->CONF_SIP. robot_type  );
printf("%-20s"," *subtype    "); printf("%-8s\n",    this->CONF_SIP. subtype );
printf("%-20s"," *serialnum  "); printf("%-8s\n",    this->CONF_SIP. serialnum   );
printf("%-20s"," rotveltop   "); printf("%-4d\n",    this->CONF_SIP. rotveltop   );
printf("%-20s"," transveltop "); printf("%-4d\n",    this->CONF_SIP. transveltop );
printf("%-20s"," *name   "); printf("%-8s\n",    this->CONF_SIP. name    );
printf("%-20s"," rotvelmax   "); printf("%-4d\n",    this->CONF_SIP. rotvelmax   );
printf("%-20s"," transvelmax "); printf("%-4d\n",    this->CONF_SIP. transvelmax );
printf("%-20s"," checksum    "); printf("%-4d\n",    this->CONF_SIP. checksum    );
}



void Command::print_Packet()
{
    const int w =8;
    stringstream ss;
   // int value;

        cout.width(w);
        cout<<"start";
        cout.width(w);
        cout<<"lenght";
        cout.width(w);
        cout<<"msg";
        cout.width(w);
        cout<<"format";
        cout.width(w);
        cout<<"data";
        cout.width(w+1);
        cout<<"checkSum";
        cout<<endl;

        cout.width(w);
        printf("%*x%x",w-2,*(this->s_head),*(this->s_head+1)); //Start Code
        cout.width(w);
        printf("%*x",w,*(this->s_head+2));                    //Length Code
        cout.width(w);
        printf("%*x",w,*(this->s_head+3));                    //Msg Code

        if (this->length!=3)
		{
            cout.width(w);
            printf("%*x",w,*(this->s_head+4));                 //Format Code
            cout.width(w);
            printf("%*x",w,*(this->pdata));                  //Content Code
            cout.width(w);
            printf("%*x",w,((uint16_t*)(this->pdata+(this->length-4)/2)[0]));   //CheckSum Code
        }
        else
		{
            cout.width(w);
            printf("%*x",w,0);                             //Format Code
            cout.width(w);
            printf("%*x",w,0);                              //Content Code
            cout.width(w);
            printf("%*x",w,*((uint16_t*)(this->s_head+4)));   //CheckSum Code
        }
        cout<<endl;
}

uint16_t Command::totalLength()
{
        if (this->length == 3)
            return 6;               //Handle exception for some commands
        else
            return 3+this->length;
}

//Clean all bytes except first two bytes(Start Code)
void Command::clean()
{
    memset(this->s_head+2,0,PACKET_SIZE-2);
}

uint8_t* Command::jump(int index)
{
        int offsetSum = 0;
        int lengthArray[] = {2,1,1,1,0,2};
        lengthArray[4] = this->length-4;

        for(uint8_t count = 0;count < index;count++)
            offsetSum = offsetSum + lengthArray[count]; 
			 
        return this->s_head+offsetSum;
}



int Command::calCheckSum(unsigned char myBuf[])					//Check Sum
{
    int i;
    unsigned char n;
    int c = 0; i = 3;
    n = myBuf[2]-2;

    while(n>1)
	{
        c+= ((unsigned char)myBuf[i]<<8)|(unsigned char)myBuf[i+1];
        c = c & 0xffff;
        n -= 2;
        i += 2;
    }
    if(n>0)
        c = c^(int)((unsigned char) myBuf[i]);
    #ifdef DEBUG
        printf("CheckSum = %4x \n",c);
    #endif
    return c;
}

void Command::swapBits()
{
    if(length==3)
	{
        //Swap CheckSum
        this->swap(this->s_head+4,this->s_head+5);
    }
	else
	{
        uint8_t *pchecksum = ((uint8_t*)this->pdata) + this->length -4;
        //Swap CheckSum
        this->swap(pchecksum,pchecksum+1);
        //Swap Data
        //this->swap((uint8_t*)this->pdata,((uint8_t*)this->pdata)+1);
    }
}


void Command::swap(uint8_t* a,uint8_t* b)
{
    uint8_t temp = *a;
    *a = *b;
    *b = temp;
}


void Command::routine()
{
    this->swapBits();
        #ifdef DEBUG
            printf("HEAD Generated");
        #endif
}

/*
uint8_t* Command::convertForSending()
{
    this->swapBits();
}
*/