#include <cstdio>
#include <cstdlib>
#include <iostream>


#ifdef ROS
	#include "ros/ros.h"
#endif
#include "serial/serial.h"
#include "commands.h"
#include "ubot.h"


#ifdef _WIN32
//WARNING:For Windows 
	#include <windows.h>
#elif _linux
	#include <unistd.h>
#endif

using namespace std;


Ubot::Ubot()
{
#ifdef __linux
	//For Linux 
	system("cd /");
 
    system("ls");
    system("sh setup.sh"); 
	
	my_serial = new serial::Serial("/dev/ttyUSB0", 9600, serial::Timeout::simpleTimeout(50));
#elif _WIN32
	//For Windows 
	my_serial = new serial::Serial("COM8", 9600, serial::Timeout::simpleTimeout(50));
#endif
}

void Ubot::initROS(int argc, char *argv[])
{

	//Add self to ROS with node name "Base_Controller"
	ros::init(argc, argv, "Base_Controller");


	static ros::NodeHandle odom_handle;
    static ros::NodeHandle pc_handle;

    static ros::Publisher  odom_Pusher;
    static ros::Publisher  pc_Pusher;

    static ros::NodeHandle geometry_handle;
    static ros::Subscriber geometry_Subscriber;  
    this->Publisher = &odom_Pusher;
    this->pc_Publisher = &pc_Pusher;
    //Confirm self to be pusher for odemetry data
	odom_Pusher = odom_handle.advertise<nav_msgs::Odometry>(topicName_odom,200);
	//Confirm self to be pusher for sonar point cloud data
    pc_Pusher = pc_handle.advertise<sensor_msgs::PointCloud>("sonar_pc",200);

    //Confirm self to be subscriber of command topic (movement command)
	geometry_Subscriber = geometry_handle.subscribe(topicName_geom,200,ros_cmd_handle);
}

bool Ubot::initUbot()					
{
	if(!this->my_serial->isOpen())
	{
		cout<<"!!!!!Port error"<<endl;
		return false;
	}

	this->my_serial->write(this->mycmd.ubot_cmd_SYNC0(),6);           			  //message   length=6

    memset(this->mycmd.r_head, 0, PACKET_SIZE);
    this->my_serial->read(this->mycmd.r_head, 20);

    if(memcmp(this->mycmd.r_head, this->mycmd.ubot_cmd_SYNC0(), 6) !=0)           //check the communication
        {
            cerr << "communication error .....restart or check code out(SYNC0)"<< endl;
            return false;
        }
    else
        cout<<"SYNC0 succeed"<<endl;
        
////////////////////////////////////////////////////////////////////////////////////////initial communication  SYNC1
    this->my_serial->write(this->mycmd.ubot_cmd_SYNC1(),6);                       //message   length=6
    memset(this->mycmd.r_head, 0, PACKET_SIZE);
    this->my_serial->read(this->mycmd.r_head, PACKET_SIZE);

    if(memcmp(this->mycmd.r_head, this->mycmd.ubot_cmd_SYNC1(), 6) !=0)           //check the communication
    {
            cerr << "communication error .....restart or check code out(SYNC1)"<< endl;
            return false;
	}
    else
        cout<<"SYNC1 succeed"<<endl;
        
////////////////////////////////////////////////////////////////////////////////////////initial communication  SYNC2
    this->my_serial->write(this->mycmd.ubot_cmd_SYNC2(),6);
    cout<<"communication setting successful!!!!!!"<<endl;
////////////////////////////////////////////////////////////////////////////////////////WTF0
    this->mycmd.ubot_cmd_WTF0();
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());
////////////////////////////////////////////////////////////////////////////////////////WTF1
    this->mycmd.ubot_cmd_WTF1();
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());
////////////////////////////////////////////////////////////////////////////////////////Parameter setting
    this->mycmd.ubot_cmd_SETV(1000);
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());                                                  //default V = 100mm/s

    this->mycmd.ubot_cmd_SETA(100);
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());                                                  //default A = 100mm/s^2

    this->mycmd.ubot_cmd_SETRV(100);
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());                                                   //default RV

    this->mycmd.ubot_cmd_ENABLE();
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());

    this->mycmd.ubot_cmd_SETO();
     this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());
////////////////////////////////////////////////////////////////////////////////////////OPEN
    this->mycmd.ubot_cmd_OPEN();
    this->my_serial->write(this->mycmd.s_head,this->mycmd.totalLength());

    return true;
}


int Ubot::ubot_msgs_scan(unsigned char* tmp)
{
    string buffer;
	int i=0;
	int count =0;
	int step=0;

	while(count<100)
	{
		// read and wait Com Port
		buffer = this->my_serial->read(1);
		if(buffer.length()==0){
			//TODO: Check the sleep API for Linux
			#ifdef _WIN32
				Sleep(20); //20 ms
			#elif _linux
				usleep(20000); //20 ms
			#endif



			printf("count:%d...\r\n", count);
			count++;
			continue;
		}
		unsigned char data = (unsigned char)buffer[0];
		//printf("data:%x\r\n", data);

		switch(step)
		{
		case 0://0xfa
			{
				if(data == 0xfa) tmp[0]=data;
				else return -1;

				step = 1;
				break;
			}
		case 1://0xfb
			{
				if(data == 0xfb) tmp[1]=data;
				else return -1;

				step = 2;
				break;
			}
		case 2://msg_length
			{
				tmp[2] = data;
				if(tmp[2]<3) return -1;

				//printf("msg_length:%d\r\n", tmp[2]);

				step = 3;
				break;
			}
		case 3://get data
			{
				if(i < tmp[2])
				{
					tmp[i+3] = data;
					//printf("tmp[%d]:%d\r\n", i+3, tmp[i+3]);
					i++;
				}

				if(i == tmp[2]){ 
					//printf("Recv data:%d byte\r\n", i);
					return 0;
				}
				break;
			}
		}
		
	}
	printf("\r\nWoring!! recvice time out!!\r\n\r\n");
    return -1;
}
