#include <cstdio>
#include <cstdlib>
#ifndef COMMANDS_H
#define COMMANDS_H
#endif

struct sip_standar
{
    uint16_t start;
    uint8_t length;
    uint8_t status;
    int16_t x_pos;
    int16_t y_pos;
    float th_pos;
    int16_t l_vel;
    int16_t r_vel;
    uint8_t battery;
    uint16_t stall_and_bumpers;
    uint16_t th_pos_360;
    uint16_t flag;
    uint8_t compass;
    uint8_t sonar_count;
    uint16_t sonar_reading0;
    uint16_t sonar_reading1;
    uint16_t sonar_reading2;
    uint16_t sonar_reading3;
    uint16_t sonar_reading4;
    uint16_t sonar_reading5;
    uint16_t sonar_reading6;
    uint16_t sonar_reading7;
    uint16_t sonar_reading8;
    uint16_t sonar_reading9;
    uint16_t sonar_reading10;
    uint16_t sonar_reading11;
    uint16_t sonar_reading12;
    uint16_t sonar_reading13;
    uint16_t sonar_reading14;
    uint16_t sonar_reading15;
    uint8_t grip_state;
    uint8_t ad_portnum;
    uint8_t ad_reading;
    uint8_t digital_input;
    uint8_t digital_output;
    uint8_t batteryx10;
    uint8_t chargestate;
    uint16_t checksum;
};


struct sip_encoder
{
	uint16_t start;
	uint8_t length;
	uint8_t type;
	uint32_t l_encoder;
	uint32_t r_encoder;
	uint16_t checksum;	
};

struct sip_config
{
	uint16_t start;
	uint16_t length;
	uint8_t type;
	uint8_t *robot_type;
	uint8_t *subtype;
	uint8_t *serialnum;
	uint16_t rotveltop;
	uint16_t transveltop;
	uint8_t *name;
	uint16_t rotvelmax;
	uint16_t transvelmax;
	uint16_t checksum;
};