#include <cstdio>
#include <cstdlib>
#include "serial/serial.h"
#include "commands.h"
#ifdef ROS
	#include "nav_msgs/Odometry.h"
	#define topicName_odom "odom"
	#define topicName_geom "cmd_vel"
#endif

//Must be implement!!!!
extern void ros_cmd_handle(const geometry_msgs::Twist& msg);


class  Ubot
{
public:
	 serial::Serial *my_serial;
	 Command mycmd; 

	#ifdef ROS
	 //Topic Publisher declear
	 ros::Publisher *Publisher;
	 ros::Publisher *pc_Publisher;
	#endif


	 //@Function : Initiate the Ubot class and open serial port 
	 Ubot();

	 //@Function : Initiate all the necessary component for ROS and add self as a node in ROS
	 void initROS(int argc, char *argv[]);

	 //@Function : Send necessary commands to intiate Ubot through COM Port 
     //@Return : Return 1 if success   return -1 if error
	 bool initUbot();

	 //@Function : Read Data From Ubot and search the packet 
	 //@Parameter: [buffer: pointer to put found packet]
     //@Return : 0 when a packet found . -1 fails to find any packet after reading 100 bytes
	 int ubot_msgs_scan(unsigned char* buffer);



	 //void send_odom_to_ros(nav_msgs::Odometry *msg);

	~ Ubot();
private:
	
	


	
};