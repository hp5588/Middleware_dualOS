#ifndef COMMANDS_H
#define COMMANDS_H

#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <cstdlib>
#include <stdint.h>
#include "commands.h"
#include "structures.h"
#include <vector>

#ifdef ROS
    #include "sensor_msgs/PointCloud.h"
    #include "nav_msgs/Odometry.h"
    #include "geometry_msgs/Twist.h"
    #include <tf/transform_broadcaster.h>
#endif 


#define sizeofInt sizeof(int)/2+4

#define HEAD_LENGTH 5  //Bytes
#define CHECKSUM_LENGTH 2  //Bytes

#define PACKET_SIZE 128 //Bytes

#define tInterger 0x3B
#define tAbsValue 0x1B
#define tString   0x2B

//Convertion Parameter
#define wheelWitdth 500.0 //mm 

#define ROTATE_STEP_ANGLE 2 //Degree
#define LINEAR_STEP_DISTANCE 50 //mm

///Scale 
#define SCALE_sonar_point 0.001


//Consnt
#define PI 3.1415926


//// start lenght msg format *data checkSum ////
////   2     1     1    1       8     2     ////
//Structure of CCP
 struct ccp_format{
    uint16_t start;
    uint8_t  lenght;
    uint8_t  msg;
    uint8_t  format;
    uint16_t *data;
    uint16_t checkSum;
};



///FORMAT///
typedef struct
{
	int16_t		x;
    int16_t		y;
    float		theta;
	double		Vx;
    double		Vy;
	double		angular_z;
}Odom_Data;


class Command {
public:
    Command();
    int length;

    //Sync Command 
    uint8_t* ubot_cmd_SYNC0();
    uint8_t* ubot_cmd_SYNC1();
    uint8_t* ubot_cmd_SYNC2();

//////IMPORTANT/////////////////////////////
    //Unknown Command - Needed to be send after SYNC0 SYNC1 SYNC3
    uint8_t* ubot_cmd_WTF0(); // <====
    uint8_t* ubot_cmd_WTF1(); // <====
////////////////////////////////////////////

    uint8_t* ubot_cmd_PULSE();
    uint8_t* ubot_cmd_OPEN();
    uint8_t* ubot_cmd_CLOSE();
    uint8_t* ubot_cmd_ENABLE();
    uint8_t* ubot_cmd_SETA(int linearAcceleration);
    uint8_t* ubot_cmd_SETV(int maxLinearSpeed);
    uint8_t* ubot_cmd_SETO();                               //Reset Relative Position to (0,0,0)
    uint8_t* ubot_cmd_MOVE(int distance);                   //mm
    uint8_t* ubot_cmd_ROTATE(int degree);                   //Rotate to specified course
    uint8_t* ubot_cmd_SETRV (int rotateSpeed);              //degree/sec
    uint8_t* ubot_cmd_VEL (int speed);                      //Linear Speed (mm/sec)
    uint8_t* ubot_cmd_HEAD (int degree);                    //Rotate to specified absolute course with max speed
    uint8_t* ubot_cmd_DHEAD (int degree);                   //Rotate to specified relative course with max speed
    uint8_t* ubot_cmd_ENCODER (bool returnVal);             // if >1 ubot will return encoder SIP
    uint8_t* ubot_cmd_RVEL (int rspeed);                    //Set turning speed
    uint8_t* ubot_cmd_SETRA (int degree);                   //Set turning acceleration
    uint8_t* ubot_cmd_SONAR (bool sonar1,bool sonar2);      //Enable Sonar
    uint8_t* ubot_cmd_STOP ();                              //Stop moving (Relay still on)
    uint8_t* ubot_cmd_E_STOP ();                            //Emegency Stop (Relay off)
    uint8_t* ubot_cmd_RESET ();                             //Software Reset
    


    //@Function : Read the data to the buffer
    //@Parameter: pass the length of data to read
    int readAsPacket(int read_length);



    //@Function : Parse the data to different type of format
    //@Return : -1 if length does not match content or CheckSum is wrong
    //          1 : std_SIP is ready 2:Config_SIP is ready 3:Encoder_SIP is ready
    //          0 none of data is ready yet
    int parse(uint8_t* pPacketData);


    ////////////////////////////////////////////
    //Naming Rules for the follwing section   //
    //ex.) ubot_ros_xxxxxxx  means ubot to ros//
    ////////////////////////////////////////////
    

   

#ifdef ROS
     //@Function : Transform the Ubot pose to ROS format [Odometry]
    int ubot_ros_parse_and_convert_odom(uint8_t* pPacketData, nav_msgs::Odometry *odom);
    
    //@Function : Transform the distance data to Pointcloud[ROS Format]
    int ubot_ros_sonar_to_pointcloud(sensor_msgs::PointCloud *theCloud);
    
    //@Function : Transform the ROS move command to Ubot format
    //@Return : cmd_queue length
    //@Other : Converted data will be stored in commands_queue in Command class
    int ros_ubot_commands_convert(const geometry_msgs::Twist& msg);

#else
    //@Function : Transform the Ubot pose to Odom_Data structure [Odometry]
    int ubot_ros_parse_and_convert_odom(uint8_t *pPacketData, Odom_Data *OutputData);

    //@Function : Transform the ROS move command to Ubot format
    //@Return : cmd_queue length
    //@Other : Converted data will be stored in commands_queue in Command class
    int ros_ubot_commands_convert(double velocity_x ,double angular_z);
#endif
    



    //@Function: jump(int index)
    ///////////////////Packet Format///////////////////
    //index     0    1     2     3     4      5    ////
    ////     start lenght msg format data checkSum ////
    ////       2     1     1     1     ?      2     ////
    ////////////////////////////////////////////////////
    //@Return pointer at specify index
        //ex.) index = 3 --> will return pointer at starting of'format'
    uint8_t* jump(int index);


    ////Debug Functions////
    //------------------------------------------//

    //@Function : Print the raw command
    void print_Raw(int limit);

    //@Function : Print the raw receive data
    void print_Raw_r(int limit);

    //@Function : Print the whole packet
    void print_Packet();
    void print_Raw_Std_SIP();
    void print_Std_SIP();
    void print_Config_SIP();
    //------------------------------------------//


    uint16_t *pdata;

    uint16_t totalLength();

    //@Function : Check Sum to verify the received data 
    int calCheckSum(unsigned char myBuf[]);

    uint8_t *s_head;
    uint8_t *r_head;

    uint8_t commands_queue[5][128];


    bool isAval_stdSIP;
    bool isAval_confSIP;
    bool isAval_encSIP;

    sip_standar STD_SIP;    //Standear SIP
    sip_encoder ENC_SIP;    //Encoder SIP
    sip_config  CONF_SIP;   //Configuration SIP


private:
    uint8_t *r_current;
    char str_robotType[10];
    char str_subType[10];
    char str_serialNum[10];
    char str_name[10];

    //@Function : Clean TX buffer except for first two bytes (0xfa 0xfb)
    void clean();

    //@Function : set "lenght", "msg" and checkSum in the packet according to msg_Num
    void setValue_l3 (int msg_Num);

    //@Function : set "lenght", "msg","data" and "checkSum" in the packet according to msg_Num
    void setPartValue(int msg_Num,int data);

    //@Function : Checksum Bytes
    void swapBits(); 

    //@Function : Swap data between two pointer
    void swap(uint8_t*,uint8_t*);

    //@Function : this function will be called at the end when generating cmds for ubot
    void routine();

    //void fill_parse();

    //Flag Section
    bool FLAG_headFounded;
    bool FLAG_fullLengthFounded;
    bool FLAG_lastIs_FA;
    bool FLAG_getLength;

};



const uint8_t ccp_startCode[2] = {250,251};
//ccp_lengthCode


//Interger Type:0x3B
//absValue Type:0x1B
//String   Type:0x2B

const uint8_t ccp_msgCode[][3] =
{
//  length      msg  format
    {3,0x00, 0},               //PULSE
    {3,0x01, 0},               //OPEN
    {3,0x02, 0},               //CLOSE
    {0,0x03, 0},               //None
    {sizeofInt,0x04, tInterger},  //ENABLE
    {sizeofInt,0x05, tInterger},  //SETA
    {sizeofInt,0x06, tInterger},  //SETV
    {3,0x07, 0},               //SETO
    {sizeofInt,0x08, tInterger},  //MOVE
    {sizeofInt,0x09, tInterger},  //ROTATE
    {sizeofInt,10,tInterger},   //SETRV
    {sizeofInt,11,tInterger},   //VEL   linear speed(mm/sec)
    {sizeofInt,12,tInterger},   //HEAD
    {sizeofInt,13,tInterger},     //DHEAD
    {0,14,0},
    {0,15,0},
    {0,16,0},
    {0,17,0},
    {0,18,0},
    {sizeofInt,19,tInterger},//ENCODER
    {0,20,0},
    {sizeofInt,21,tInterger},//RVEL
    {0,22,0},
    {sizeofInt,23,tInterger},//SETRA
    {0,24,0},
    {0,25,0},
    {0,26,0},
    {0,27,0},
    {sizeofInt,28,tInterger},     //SONAR
    {3,29,0},//STOP
{	sizeofInt	,	30	,	tInterger	}	,
{	sizeofInt	,	31	,	tInterger	}	,
{	sizeofInt	,	32	,	tInterger	}	,
{	sizeofInt	,	33	,	tInterger	}	,
{	sizeofInt	,	34	,	tInterger	}	,
{	sizeofInt	,	35	,	tInterger	}	,
{	sizeofInt	,	36	,	tInterger	}	,
{	sizeofInt	,	37	,	tInterger	}	,
{	sizeofInt	,	38	,	tInterger	}	,
{	sizeofInt	,	39	,	tInterger	}	,
{	sizeofInt	,	40	,	tInterger	}	,
{	sizeofInt	,	41	,	tInterger	}	,
{	sizeofInt	,	42	,	tInterger	}	,
{	sizeofInt	,	43	,	tInterger	}	,
{	sizeofInt	,	44	,	tInterger	}	,
{	sizeofInt	,	45	,	tInterger	}	,
{	sizeofInt	,	46	,	tInterger	}	,
{	sizeofInt	,	47	,	tInterger	}	,
{	sizeofInt	,	48	,	tInterger	}	,
{	sizeofInt	,	49	,	tInterger	}	,
{	sizeofInt	,	50	,	tInterger	}	,
{	sizeofInt	,	51	,	tInterger	}	,
{	sizeofInt	,	52	,	tInterger	}	,
{	sizeofInt	,	53	,	tInterger	}	,
{	sizeofInt	,	54	,	tInterger	}	,
{	3	,	55	,	0	}	, //E_STOP
{	sizeofInt	,	56	,	tInterger	}	,
{	sizeofInt	,	57	,	tInterger	}	,
{	sizeofInt	,	58	,	tInterger	}	,
{	sizeofInt	,	59	,	tInterger	}	,
{	sizeofInt	,	60	,	tInterger	}	,
{	sizeofInt	,	61	,	tInterger	}	,
{	sizeofInt	,	62	,	tInterger	}	,
{	sizeofInt	,	63	,	tInterger	}	,
{	sizeofInt	,	64	,	tInterger	}	,
{	sizeofInt	,	65	,	tInterger	}	,
{	sizeofInt	,	66	,	tInterger	}	,
{	sizeofInt	,	67	,	tInterger	}	,
{	sizeofInt	,	68	,	tInterger	}	,
{	sizeofInt	,	69	,	tInterger	}	,
{	sizeofInt	,	70	,	tInterger	}	,
{	sizeofInt	,	71	,	tInterger	}	,
{	sizeofInt	,	72	,	tInterger	}	,
{	sizeofInt	,	73	,	tInterger	}	,
{	sizeofInt	,	74	,	tInterger	}	,
{	sizeofInt	,	75	,	tInterger	}	,
{	sizeofInt	,	76	,	tInterger	}	,
{	sizeofInt	,	77	,	tInterger	}	,
{	sizeofInt	,	78	,	tInterger	}	,
{	sizeofInt	,	79	,	tInterger	}	,
{	sizeofInt	,	80	,	tInterger	}	,
{	sizeofInt	,	81	,	tInterger	}	,
{	sizeofInt	,	82	,	tInterger	}	,
{	sizeofInt	,	83	,	tInterger	}	,
{	sizeofInt	,	84	,	tInterger	}	,
{	sizeofInt	,	85	,	tInterger	}	,
{	sizeofInt	,	86	,	tInterger	}	,
{	sizeofInt	,	87	,	tInterger	}	,
{	sizeofInt	,	88	,	tInterger	}	,
{	sizeofInt	,	89	,	tInterger	}	,
{	sizeofInt	,	90	,	tInterger	}	,
{	sizeofInt	,	91	,	tInterger	}	,
{	sizeofInt	,	92	,	tInterger	}	,
{	sizeofInt	,	93	,	tInterger	}	,
{	sizeofInt	,	94	,	tInterger	}	,
{	sizeofInt	,	95	,	tInterger	}	,
{	sizeofInt	,	96	,	tInterger	}	,
{	sizeofInt	,	97	,	tInterger	}	,
{	sizeofInt	,	98	,	tInterger	}	,
{	sizeofInt	,	99	,	tInterger	}	,
{	sizeofInt	,	100	,	tInterger	}	,
{	sizeofInt	,	101	,	tInterger	}	,
{	sizeofInt	,	102	,	tInterger	}	,
{	sizeofInt	,	103	,	tInterger	}	,
{	sizeofInt	,	104	,	tInterger	}	,
{	sizeofInt	,	105	,	tInterger	}	,
{	sizeofInt	,	106	,	tInterger	}	,
{	sizeofInt	,	107	,	tInterger	}	,
{	sizeofInt	,	108	,	tInterger	}	,
{	sizeofInt	,	109	,	tInterger	}	,
{	sizeofInt	,	110	,	tInterger	}	,
{	sizeofInt	,	111	,	tInterger	}	,
{	sizeofInt	,	112	,	tInterger	}	,
{	sizeofInt	,	113	,	tInterger	}	,
{	sizeofInt	,	114	,	tInterger	}	,
{	sizeofInt	,	115	,	tInterger	}	,
{	sizeofInt	,	116	,	tInterger	}	,
{	sizeofInt	,	117	,	tInterger	}	,
{	sizeofInt	,	118	,	tInterger	}	,
{	sizeofInt	,	119	,	tInterger	}	,
{	sizeofInt	,	120	,	tInterger	}	,
{	sizeofInt	,	121	,	tInterger	}	,
{	sizeofInt	,	122	,	tInterger	}	,
{	sizeofInt	,	123	,	tInterger	}	,
{	sizeofInt	,	124	,	tInterger	}	,
{	sizeofInt	,	125	,	tInterger	}	,
{	sizeofInt	,	126	,	tInterger	}	,
{	sizeofInt	,	127	,	tInterger	}	,
{	sizeofInt	,	128	,	tInterger	}	,
{	sizeofInt	,	129	,	tInterger	}	,
{	sizeofInt	,	130	,	tInterger	}	,
{	sizeofInt	,	131	,	tInterger	}	,
{	sizeofInt	,	132	,	tInterger	}	,
{	sizeofInt	,	133	,	tInterger	}	,
{	sizeofInt	,	134	,	tInterger	}	,
{	sizeofInt	,	135	,	tInterger	}	,
{	sizeofInt	,	136	,	tInterger	}	,
{	sizeofInt	,	137	,	tInterger	}	,
{	sizeofInt	,	138	,	tInterger	}	,
{	sizeofInt	,	139	,	tInterger	}	,
{	sizeofInt	,	140	,	tInterger	}	,
{	sizeofInt	,	141	,	tInterger	}	,
{	sizeofInt	,	142	,	tInterger	}	,
{	sizeofInt	,	143	,	tInterger	}	,
{	sizeofInt	,	144	,	tInterger	}	,
{	sizeofInt	,	145	,	tInterger	}	,
{	sizeofInt	,	146	,	tInterger	}	,
{	sizeofInt	,	147	,	tInterger	}	,
{	sizeofInt	,	148	,	tInterger	}	,
{	sizeofInt	,	149	,	tInterger	}	,
{	sizeofInt	,	150	,	tInterger	}	,
{	sizeofInt	,	151	,	tInterger	}	,
{	sizeofInt	,	152	,	tInterger	}	,
{	sizeofInt	,	153	,	tInterger	}	,
{	sizeofInt	,	154	,	tInterger	}	,
{	sizeofInt	,	155	,	tInterger	}	,
{	sizeofInt	,	156	,	tInterger	}	,
{	sizeofInt	,	157	,	tInterger	}	,
{	sizeofInt	,	158	,	tInterger	}	,
{	sizeofInt	,	159	,	tInterger	}	,
{	sizeofInt	,	160	,	tInterger	}	,
{	sizeofInt	,	161	,	tInterger	}	,
{	sizeofInt	,	162	,	tInterger	}	,
{	sizeofInt	,	163	,	tInterger	}	,
{	sizeofInt	,	164	,	tInterger	}	,
{	sizeofInt	,	165	,	tInterger	}	,
{	sizeofInt	,	166	,	tInterger	}	,
{	sizeofInt	,	167	,	tInterger	}	,
{	sizeofInt	,	168	,	tInterger	}	,
{	sizeofInt	,	169	,	tInterger	}	,
{	sizeofInt	,	170	,	tInterger	}	,
{	sizeofInt	,	171	,	tInterger	}	,
{	sizeofInt	,	172	,	tInterger	}	,
{	sizeofInt	,	173	,	tInterger	}	,
{	sizeofInt	,	174	,	tInterger	}	,
{	sizeofInt	,	175	,	tInterger	}	,
{	sizeofInt	,	176	,	tInterger	}	,
{	sizeofInt	,	177	,	tInterger	}	,
{	sizeofInt	,	178	,	tInterger	}	,
{	sizeofInt	,	179	,	tInterger	}	,
{	sizeofInt	,	180	,	tInterger	}	,
{	sizeofInt	,	181	,	tInterger	}	,
{	sizeofInt	,	182	,	tInterger	}	,
{	sizeofInt	,	183	,	tInterger	}	,
{	sizeofInt	,	184	,	tInterger	}	,
{	sizeofInt	,	185	,	tInterger	}	,
{	sizeofInt	,	186	,	tInterger	}	,
{	sizeofInt	,	187	,	tInterger	}	,
{	sizeofInt	,	188	,	tInterger	}	,
{	sizeofInt	,	189	,	tInterger	}	,
{	sizeofInt	,	190	,	tInterger	}	,
{	sizeofInt	,	191	,	tInterger	}	,
{	sizeofInt	,	192	,	tInterger	}	,
{	sizeofInt	,	193	,	tInterger	}	,
{	sizeofInt	,	194	,	tInterger	}	,
{	sizeofInt	,	195	,	tInterger	}	,
{	sizeofInt	,	196	,	tInterger	}	,
{	sizeofInt	,	197	,	tInterger	}	,
{	sizeofInt	,	198	,	tInterger	}	,
{	sizeofInt	,	199	,	tInterger	}	,
{	sizeofInt	,	200	,	tInterger	}	,
{	sizeofInt	,	201	,	tInterger	}	,
{	sizeofInt	,	202	,	tInterger	}	,
{	sizeofInt	,	203	,	tInterger	}	,
{	sizeofInt	,	204	,	tInterger	}	,
{	sizeofInt	,	205	,	tInterger	}	,
{	sizeofInt	,	206	,	tInterger	}	,
{	sizeofInt	,	207	,	tInterger	}	,
{	sizeofInt	,	208	,	tInterger	}	,
{	sizeofInt	,	209	,	tInterger	}	,
{	sizeofInt	,	210	,	tInterger	}	,
{	sizeofInt	,	211	,	tInterger	}	,
{	sizeofInt	,	212	,	tInterger	}	,
{	sizeofInt	,	213	,	tInterger	}	,
{	sizeofInt	,	214	,	tInterger	}	,
{	sizeofInt	,	215	,	tInterger	}	,
{	sizeofInt	,	216	,	tInterger	}	,
{	sizeofInt	,	217	,	tInterger	}	,
{	sizeofInt	,	218	,	tInterger	}	,
{	sizeofInt	,	219	,	tInterger	}	,
{	sizeofInt	,	220	,	tInterger	}	,
{	sizeofInt	,	221	,	tInterger	}	,
{	sizeofInt	,	222	,	tInterger	}	,
{	sizeofInt	,	223	,	tInterger	}	,
{	sizeofInt	,	224	,	tInterger	}	,
{	sizeofInt	,	225	,	tInterger	}	,
{	sizeofInt	,	226	,	tInterger	}	,
{	sizeofInt	,	227	,	tInterger	}	,
{	sizeofInt	,	228	,	tInterger	}	,
{	sizeofInt	,	229	,	tInterger	}	,
{	sizeofInt	,	230	,	tInterger	}	,
{	sizeofInt	,	231	,	tInterger	}	,
{	sizeofInt	,	232	,	tInterger	}	,
{	sizeofInt	,	233	,	tInterger	}	,
{	sizeofInt	,	234	,	tInterger	}	,
{	sizeofInt	,	235	,	tInterger	}	,
{	sizeofInt	,	236	,	tInterger	}	,
{	sizeofInt	,	237	,	tInterger	}	,
{	sizeofInt	,	238	,	tInterger	}	,
{	sizeofInt	,	239	,	tInterger	}	,
{	sizeofInt	,	240	,	tInterger	}	,
{	sizeofInt	,	241	,	tInterger	}	,
{	sizeofInt	,	242	,	tInterger	}	,
{	sizeofInt	,	243	,	tInterger	}	,
{	sizeofInt	,	244	,	tInterger	}	,
{	sizeofInt	,	245	,	tInterger	}	,
{	sizeofInt	,	246	,	tInterger	}	,
{	sizeofInt	,	247	,	tInterger	}	,
{	sizeofInt	,	248	,	tInterger	}	,
{	sizeofInt	,	249	,	tInterger	}	,
{	sizeofInt	,	250	,	tInterger	}	,
{	sizeofInt	,	251	,	tInterger	}	,
{	sizeofInt	,	252	,	tInterger	}	,
{	3	,	253	,	0	}



};


#endif
#endif
